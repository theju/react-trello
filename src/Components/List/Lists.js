import React from 'react';
import Api from '../Data/getDataFromTrello';
import ListInput from './ListInput';
import ListAddBtn from './ListAddBtn';
import Cards from "../Cards/Cards";
import '../../../node_modules/font-awesome/css/font-awesome.min.css';


class Lists extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showInput: false,
            inputName:"",
            ListData: [],
        }
    }

    handleClose = () => {
        this.setState({
            showInput: false
        })
    }

    handleClick = () => {
        this.setState({
            showInput: true
        })
    }

    getInputValue = (inputName) => {
            Api.createTrelloList(inputName)
            .then(response => response.json())
            .then(data => {
                console.log(data, "data");
                this.setState({
                    ListData: [...this.state.ListData, data],
                })
            })  
            .catch(console.log)
    }

    deleteList(id) {
        console.log(id, " delete id")
        Api.deleteListFromTheBoard(id)
            .then(response => response.json())
            .then(data => {
                console.log(data, " id")
                let newList = [];
                newList = this.state.ListData.filter(list => list.id !== id)
                this.setState({
                    ListData: newList
                })

            })
    }

    componentDidMount() {
        Api.getTrelloList().then(resp => resp.json())
            .then(data => {
                console.log(data)
                this.setState({
                    ListData: data
                } ,()=>{console.log()})
            });
    }

    render() {
        return(<div className = "List" style = {{display:"flex", justifyContent:"start"}}>
            {this.state.ListData.map(list => {
                    return (
                        <div className="lists-container" key={list.id}>
                            <div className = "lists">
                            <div className="ListHead">
                                <h3>{list.name}</h3>
                                <div>
                                    <i className="fa fa-trash" style = {{"cursor": "pointer"}} onClick={this.deleteList.bind(this, list.id)}></i>
                                </div>
                            </div>
                                <Cards id = {list.id}/>
                            </div>
                        </div>
                    );
                })}
            <div className="input-button" >
                {this.state.showInput ? <ListInput onClickClose={this.handleClose}  getInputValue={this.getInputValue} /> : <ListAddBtn clickEvent={this.handleClick}/>}       
            </div>
        </div>);
    }
}

export default Lists;