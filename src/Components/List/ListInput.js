import React from "react";

const style = {
    "cursor":"pointer"
}

class ListInput extends React.Component{
    constructor(props){
        super(props);
        this.state={
            listName : ""
        }
    }

    getName = (e) => {
        console.log(e.target.value);
        this.setState({
            listName: e.target.value
        })
    }

    passDatatoAddList = (input) => {
        this.props.getInputValue(input);
        this.setState({
            listName : ""
        })
    }
    
    render(){
    return (<div className="create-list">
            <form className="createList" style = {style} onSubmit={(e) => e.preventDefault()}>
                <input onChange={this.getName} value={this.state.listName} type="text" /> 
            </form>
            <div className="extra-content">
            <input type="submit" style = {style} value="Add list" className="create-btn" onClick={() => this.passDatatoAddList(this.state.listName)}/>
            <p className="close" style = {style} onClick={() => this.props.onClickClose()}>X</p>
            </div>
    </div>);
    }
}

export default ListInput;