import React from "react";
import CheckListContent from '../CheckList/CheckListContent';

const style = {
  cursor:"pointer", 
}

class CardElement extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checklistPopUp: false
    };

    this.hideCheckList = this.hideCheckList.bind(this);
    this.showChecklist = this.showChecklist.bind(this);
  }
  showChecklist = (id) => {
    this.setState({
      checklistPopUp: true
    });
  };
  hideCheckList = (id) => {
    console.log(id)
    this.setState({ checklistPopUp: false });
  };
 
  render() {
    return (
      <div>
      <div className = "cards">
          <p className = "name"  onClick={this.showChecklist.bind(this, this.props.id)}>{this.props.name}</p>
        <i className = "fa fa-trash" style = {style}  onClick={this.props.deleteCard}></i>
      </div>
      {this.state.checklistPopUp ? (
          <CheckListContent
            checklistclose={this.hideCheckList.bind(this, this.props.id)}
            cardId={this.props.id}
            cardName={this.props.name}
          />
        ) : null}
      </div>
    );
  }
}
export default CardElement;
