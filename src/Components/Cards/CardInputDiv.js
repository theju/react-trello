import React from "react";

const style = {
    borderColor:"snow",
    "borderRadius":"3px",
    "width":"-webkit-fill-available"
}

const cursorStyle = {
    cursor: "pointer"
}

class CardInputDiv extends React.Component {
    constructor(props){
        super(props);
        this.state ={
            newCard : ""
        }
    }
    getCardInput = (e) => {
        console.log(e.target.value)
        this.setState({
            newCard: e.target.value
        })
    }
    passDatatoAddCard = (input) => {
        this.props.addCard(input);
        this.setState({
            newCard : ""
        })
    }
    render(){
        return (
            <div className="card-holder">
                <div className="card">
                    <textarea type="text" style = {style}  value={this.state.newCard} onChange={this.getCardInput} onSubmit={(e) => e.preventDefault()} placeholder="Add a new card here" />
                </div>
                <div className="card-content">
                    <input type="submit" style = {cursorStyle} onClick={() => this.passDatatoAddCard(this.state.newCard)} value="Add a card" className="btn" />
                    <p className="close-card" style = {cursorStyle} onClick={() => this.props.onClickclose()} >X</p>
                </div>
            </div>
        )
    }
}

export default CardInputDiv;