import React from 'react';
import Api from '../Data/getDataFromTrello';
import CardElement from "../Cards/CardElement";
import CardInput from "./CardInput";
import '../../../node_modules/font-awesome/css/font-awesome.min.css';

class GetCards extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            cards: [],
        }
        this.updateCardtoList = this.updateCardtoList.bind(this)
    }
    componentDidMount() {
        this.getCardOfList()
    }

    updateCardtoList(newCard) {
        this.setState({
            cards: [...this.state.cards, newCard]
        })
    }
    
    getCardOfList() {
        Api.getCards(this.props.id)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    cards: data
                })  
            })
            .catch(error => console.log('parsed failed', error))
    }

    deleteCardFromTheList(id) {
        Api.deleteCard(id)
            .then(response => response.json())
            .then(data => {
                let updatedCardList = this.state.cards.filter(card => id !== card.id)
                this.setState({
                    cards: updatedCardList
                })
            })
            .catch(error => console.log('parsed failed', error))
    }

    render() {
        
        return (
            <div className="list-div">
                {this.state.cards.map((card, i) => {
                    return (
                        <div key={i}>
                            <CardElement id={card.id} name={card.name} deleteCard={this.deleteCardFromTheList.bind(this, card.id)}></CardElement>
                        </div>
                        );
                })}
                <CardInput updateCardList={this.updateCardtoList} id={this.props.id} />
            </div>
            );
    }
}
export default GetCards;
