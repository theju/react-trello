import React from 'react'
import Api from '../Data/getDataFromTrello';
import CardInputDiv from './CardInputDiv';

class CardInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showCard: false,
        }
    }

    handleClose = () => {
        this.setState({
            showCard:false
        })
    }

    AddCard = (newCard) => {
        Api.createCard(newCard, this.props.id)
            .then(response => response.json())
            .then(data => {
                this.props.updateCardList(data)
            })
            .catch(error => console.log('parsed failed', error))
    }

    handleClick = (e) => {
        this.setState({
            showCard: true, 
        })
    }

    render() {
        return (
            <div className="input-button">
                {this.state.showCard ? <CardInputDiv addCard={this.AddCard} onClickclose={this.handleClose}/>: 
                <p onClick={this.handleClick} className="head-body" style = {{"cursor": "pointer"}}><span>+</span> Add Card</p>
                }
            </div>
        )
    }
}

export default CardInput;