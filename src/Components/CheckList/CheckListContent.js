import React from 'react'
import Checklist from './CheckList'
import Api from '../Data/getDataFromTrello';
import '../../../node_modules/font-awesome/css/font-awesome.min.css';

export default class CardContent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            checklist: [],
            show: false,
            newCheckList: ''
        }
    }

    componentDidMount() {
        this.getCheckList()
    }

    getCheckList() {
        Api.getCheckList(this.props.cardId)
            .then(response => response.json())
            .then(data => {
                console.log(data);
                this.setState({
                    checklist: data.checklists
                })
            })
    }
   
    handleKeyPress = e => {
        if (e.key === "Enter") {
            console.log(this.state.newCheckList)
            this.postCheckList()
            this.setState({
                newCheckList: ""
            })
        }
    }

    postCheckList() {
        Api.postRequestForCheckList(this.props.cardId, this.state.newCheckList)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    checklist: [...this.state.checklist, data]
                })
            })
    }

    handleChange = e => {
        console.log(e.target.value)
        this.setState({
            newCheckList: e.target.value
        })
    }

    render() {
        return (
            <div className="checklist-popup">
                <div className="modal-content animate" >
                    <div className="checkList-Header">
                        <h1>{this.props.cardName}</h1>
                        <i onClick={this.props.checklistclose} className="fas fa-times"></i>
                    </div>
                    <div className="checkListItem-container">
                    {this.state.checklist.map((checklist, i) => {
                        return (
                            <div style={{lineHeight:"23px"}} key={i}>
                                <div className="checklist">
                                    <h2>{checklist.name}</h2>
                                </div>
                                <Checklist checklistid={checklist.id} cardid={this.props.cardId}></Checklist>
                            </div>
                        )
                    })
                    }
                    </div>
                    <div className="input-button add-checklist">
                        <textarea className="add-checklist"
                            type="text"
                            id="add-card"
                            value={this.state.newCheckList}
                            placeholder="+Add  checklist"
                            onKeyPress={this.handleKeyPress}
                            onChange={this.handleChange}
                        />
                    </div>
             </div>

            </div>

        )
    }
}