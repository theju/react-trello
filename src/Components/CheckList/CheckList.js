import React from 'react'
import Api from '../Data/getDataFromTrello';
import CheckListItem from './CheckListItem';
import CheckListBtn from "./CheckListBtn";

class CheckList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            checklistItem: [],
            newItem: '',
            checked: false,
            checklistinput: false
        }
    }
    componentDidMount() {
        this.getCheckListItem();
    }

    getCheckListItem() {
        Api.getRequestForCheckListItem(this.props.checklistid)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    checklistItem: data
                })
            })
    }
    
    handleClose = () => {
        this.setState({
            checklistinput: false
        })
    }

    handleClick = () => {
        this.setState({
            checklistinput: true
        })
    }

    handleKeyPress = e => {
        if (e.key === "Enter") {
            this.postCheckListItem()
            this.setState({
                newItem: ""
            })
        }
    }

    postCheckListItem = (newItem) => {
        console.log(newItem, this.props.checklistid )
        Api.postRequestForcheckListItem(this.props.checklistid,newItem,this.state.checked) 
            .then(response => response.json())
            .then(data => {
                let addedItem = this.state.checklistItem.map(item => item)
                addedItem.push(data)
                this.setState({
                    checklistItem: addedItem
                })
            })
    }
    handleChange = e => {
        this.setState({
            newItem: e.target.value
        })
    }

    render() {
        return (
            <div>
                {this.state.checklistItem.map(item => {
                    return (
                        <div className="check-item" key={item.id}>
                            <CheckListItem id={item.id} state={item.state} name={item.name} cardId={this.props.cardid} checklistId={this.props.checklistid}></CheckListItem>
                        </div>
                    )
                })}
                <div className="input-button">
                    {this.state.checklistinput? <CheckListBtn onClickClose={this.handleClose}  postCheckListItem={this.postCheckListItem}/>:<button className="checklistAddBtn" onClick= {this.handleClick}>+Add an Item</button>}
                </div>
            </div>
        )
    }
}

 export default CheckList;