import React from 'react'
import Api from '../Data/getDataFromTrello';

const style = {
    color: "#6b808c",
    fontStyle: "italic"
}
export default class CheckListItem extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            checked: '',
            itemState: this.props.state,
        }
        this.itemStateChecker = this.itemStateChecker.bind(this)
    }

    componentDidMount() {
        this.itemStateChecker()
    }

    itemStateChecker() {
        if (this.state.itemState === "incomplete")
            this.setState({
                checked: false,
            })
        else
            this.setState({
                checked: true,
                
            })
    }
    
    itemStateUpdater() {
        let newState = (this.state.itemState === "incomplete") ? "complete" : "incomplete";
        let checkstate = (newState === "incomplete") ? false : true
            Api.putrequestForCheckListItem(this.props.cardId,this.props.id,this.props.checklistId,newState)
                .then(response => response.json())
                .then(data => {
                    this.setState({
                        checked: checkstate,
                        itemState: newState
                    })
                })
                .catch(error => console.log(error));
    }
    render() {
            var statusCheck = !this.state.checked ? false : true
            console.log("entered")
        return (
            <div className="check-item">
                <input type="checkbox" checked={statusCheck} onChange={this.itemStateUpdater.bind(this)} ></input>
                {!this.state.checked ? <p style={style}>{this.props.name}</p> : <strike style={style}><p style={{margin:"0px"}}>{this.props.name}</p></strike>}
            </div>
        )
    }
}
