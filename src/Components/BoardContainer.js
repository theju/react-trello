import React from 'react';
import Api from '../Components/Data/getDataFromTrello';
import Lists from './List/Lists';

class BoardContainer extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            boardData: '',
        }
    }
    componentDidMount() {
        Api.getData().then(resp => resp.json())
        .then(data => { 
            this.setState({
                boardData: data
            })
        })
    }
    render(){
        const {name} = this.state.boardData;
        return(
            <div>
                <div style = {{lineHeight:"100px"}}>
                    <h1>{name}</h1>
                </div>
                <Lists/>
            </div>
        );
    }
}

export default BoardContainer;