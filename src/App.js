import React, { Component } from 'react';
import './App.css';
import Board from './Components/BoardContainer'

class App extends Component {
  render() {
    return (
      <div className="App overflow-auto main-container">
        <Board />
      </div>
    );
  }
}

export default App;
